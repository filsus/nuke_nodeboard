![compatibility](https://img.shields.io/badge/compatibility-Nuke%2FNukeX%2012%2C13-green)

***NUKE NODE BOARD***
This tool offers the user a more accesible and intuitive ToolSet node board that you can dock inside nuke pane.
****************************************************************************************************************
Last modified: 01/09/2022
Version 2.0
*********************************************************************
Nuke compability test: Nuke/NukeX 11,12,13
*********************************************************************

HOW TO INSTALL AND RUN:
**********************************************************************************************************************
1. Navigate to you /.nuke directory (this should be automatically created after Nuke startup)

2. Copy the 'NodeBoard' folder into your /.nuke directory

3. Open/create a text file called init.py and add the following two lines

import nuke
nuke.pluginAddPath('NodeBoard') 

4. Open Nuke

5. Create a new pane or dock into existing one using _Windows->Custom->NodeBoard_

6. The tool will now be docked into you nuke workspace

***********************************************************************************************************************

Please report all bugs to the email adresses below.

Thanks for using the tool!

CREATED BY:
****************************
suska.filip@gmail.com
lukas.danko1@gmail.com
****************************

SPECIAL THANKS TO:
****************************
Lars Wemmje



